package com.balush.model;

import com.balush.model.comparator.KnifeComparator;
import com.balush.model.filechecker.ExtensionChecker;
import com.balush.model.parser.dom.DOMParserUser;
import com.balush.model.parser.sax.SAXParserUser;
import com.balush.model.parser.stax.StAXReader;
import com.balush.view.View;

import java.io.File;
import java.util.List;

public class Model {
    private View view;

    public Model(View view) {
        this.view = view;
    }

    public void startModel() {
        File xml = new File("C:\\Users\\User\\IdeaProjects\\task11_XML" +
                "\\src\\main\\resources\\xml\\knifesXML.xml");
        File xsd = new File("C:\\Users\\User\\IdeaProjects\\task11_XML" +
                "\\src\\main\\resources\\xml\\knifesXSD.xsd");

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            printList(SAXParserUser.parseKnife(xml, xsd), "SAX");
            printList(StAXReader.parseKnife(xml), "StAX");
            printList(DOMParserUser.getKnifeList(xml, xsd), "DOM");
        }
    }

    private boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private void printList(List<Knife> knives, String parserName) {
        knives.sort(new KnifeComparator());
        view.showMessage(parserName);
        for (Knife knife : knives) {
            view.showMessage(knife.toString());
        }
    }
}
