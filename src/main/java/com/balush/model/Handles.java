package com.balush.model;

public class Handles {
    private String handleType;
    private String handleSubType;

    public Handles() {
    }

    public Handles(String handleType, String handleSubType) {
        this.handleType = handleType;
        this.handleSubType = handleSubType;
    }

    public String getHandleType() {
        return handleType;
    }

    public void setHandleType(String handleType) {
        this.handleType = handleType;
    }

    public String getHandleSubType() {
        return handleSubType;
    }

    public void setHandleSubType(String handleSubType) {
        this.handleSubType = handleSubType;
    }

    @Override
    public String toString() {
        return  "Handle Type: " + handleType + "\n" +
                "Handle subtype: " + handleSubType + "\n";
    }
}
