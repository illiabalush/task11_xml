package com.balush.model.parser.stax;

import com.balush.model.Handles;
import com.balush.model.Knife;
import com.balush.model.Visual;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {
    public static List<Knife> parseKnife(File xml) {
        List<Knife> knives = new ArrayList<>();
        Knife knife = null;
        Visual visual = null;
        Handles handles = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "knife":
                            knife = new Knife();
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert knife != null;
                            knife.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "handly":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert knife != null;
                            knife.setHandly(xmlEvent.asCharacters().getData());
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert knife != null;
                            knife.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "visual":
                            xmlEvent = xmlEventReader.nextEvent();
                            visual = new Visual();
                            break;
                        case "length":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visual != null;
                            visual.setLength(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "width":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visual != null;
                            visual.setWidth(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "material":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visual != null;
                            visual.setMaterial(xmlEvent.asCharacters().getData());
                            break;
                        case "handles":
                            xmlEvent = xmlEventReader.nextEvent();
                            handles = new Handles();
                            break;
                        case "handlesType":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert handles != null;
                            handles.setHandleType(xmlEvent.asCharacters().getData());
                            break;
                        case "handleSubtype":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert handles != null;
                            handles.setHandleSubType(xmlEvent.asCharacters().getData());
                            break;
                        case "bloodFlows":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visual != null;
                            visual.setBloodFlows(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert knife != null;
                            knife.setValue(xmlEvent.asCharacters().getData());
                            assert visual != null;
                            visual.setHandles(handles);
                            knife.setVisual(visual);
                            break;
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("knife")) {
                        knives.add(knife);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return knives;
    }
}
