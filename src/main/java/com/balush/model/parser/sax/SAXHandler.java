package com.balush.model.parser.sax;

import com.balush.model.Handles;
import com.balush.model.Knife;
import com.balush.model.Visual;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Knife> knives = new ArrayList<>();
    private Knife knife;
    private Visual visual;
    private Handles handles;

    private boolean knifeType = false;
    private boolean knifeHandly = false;
    private boolean knifeOrigin = false;
    private boolean knifeLength = false;
    private boolean knifeWidth = false;
    private boolean knifeMaterial = false;
    private boolean knifeHandlesType = false;
    private boolean knifeHandleSubType = false;
    private boolean knifeBloodFlows = false;
    private boolean knifeValue = false;

    public List<Knife> getKnives() {
        return this.knives;
    }

    public void startElement(String url, String localName, String qName, Attributes attributes) {
        if (qName.equalsIgnoreCase("knife")) {
            knife = new Knife();
        } else if (qName.equalsIgnoreCase("type")) {
            knifeType = true;
        } else if (qName.equalsIgnoreCase("handly")) {
            knifeHandly = true;
        } else if (qName.equalsIgnoreCase("origin")) {
            knifeOrigin = true;
        } else if (qName.equalsIgnoreCase("visual")) {
            visual = new Visual();
        } else if (qName.equalsIgnoreCase("length")) {
            knifeLength = true;
        } else if (qName.equalsIgnoreCase("width")) {
            knifeWidth = true;
        } else if (qName.equalsIgnoreCase("material")) {
            knifeMaterial = true;
        } else if (qName.equalsIgnoreCase("handles")) {
            handles = new Handles();
        } else if (qName.equalsIgnoreCase("handlesType")) {
            knifeHandlesType = true;
        } else if (qName.equalsIgnoreCase("handleSubtype")) {
            knifeHandleSubType = true;
        } else if (qName.equalsIgnoreCase("bloodFlows")) {
            knifeBloodFlows = true;
        } else if (qName.equalsIgnoreCase("value")) {
            knifeValue = true;
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("knife")) {
            visual.setHandles(handles);
            knife.setVisual(visual);
            knives.add(knife);
        }
    }

    public void characters(char ch[], int start, int length) {
        if (knifeType) {
            knife.setType(new String(ch, start, length));
            knifeType = false;
        } else if (knifeHandly) {
            knife.setHandly(new String(ch, start, length));
            knifeHandly = false;
        } else if (knifeOrigin) {
            knife.setOrigin(new String(ch, start, length));
            knifeOrigin = false;
        } else if (knifeLength) {
            visual.setLength(Integer.parseInt(new String(ch, start, length)));
            knifeLength = false;
        } else if (knifeWidth) {
            visual.setWidth(Integer.parseInt(new String(ch, start, length)));
            knifeWidth = false;
        } else if (knifeMaterial) {
            visual.setMaterial(new String(ch, start, length));
            knifeMaterial = false;
        } else if (knifeHandlesType) {
            handles.setHandleType(new String(ch, start, length));
            knifeHandlesType = false;
        } else if (knifeHandleSubType) {
            handles.setHandleSubType(new String(ch, start, length));
            knifeHandleSubType = false;
        } else if (knifeBloodFlows) {
            visual.setBloodFlows(Boolean.parseBoolean(new String(ch, start, length)));
            knifeBloodFlows = false;
        } else if (knifeValue) {
            knife.setValue(new String(ch, start, length));
            knifeValue = false;
        }
    }
}

