package com.balush.model.parser.dom;

import com.balush.model.Handles;
import com.balush.model.Knife;
import com.balush.model.Visual;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Knife> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Knife> knifes = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("knife");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Knife knife = new Knife();
            Visual visual;
            Handles handles;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                knife.setType(element.getElementsByTagName("type").item(0).getTextContent());
                knife.setHandly(element.getElementsByTagName("handly").item(0).getTextContent());
                knife.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
                visual = getVisual(element.getElementsByTagName("visual"));
                handles = getHandles(element.getElementsByTagName("handles"));
                knife.setValue(element.getElementsByTagName("value").item(0).getTextContent());
                visual.setHandles(handles);
                knife.setVisual(visual);
                knifes.add(knife);
            }
        }
        return knifes;
    }

    private Visual getVisual(NodeList nodes) {
        Visual visual = new Visual();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            visual.setLength(Integer.parseInt(element.getElementsByTagName("length").item(0).getTextContent()));
            visual.setWidth(Integer.parseInt(element.getElementsByTagName("width").item(0).getTextContent()));
            visual.setMaterial(element.getElementsByTagName("material").item(0).getTextContent());
            visual.setBloodFlows(Boolean.parseBoolean(element.getElementsByTagName("bloodFlows")
                    .item(0).getTextContent()));
        }
        return visual;
    }

    private Handles getHandles(NodeList nodes) {
        Handles handles = new Handles();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            handles.setHandleType(element.getElementsByTagName("handlesType").item(0).getTextContent());
            handles.setHandleSubType(element.getElementsByTagName("handleSubtype").item(0).getTextContent());
        }
        return handles;
    }
}
