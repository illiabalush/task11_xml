package com.balush.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class View {
    protected static Logger logger = LogManager.getLogger(View.class);

    public abstract void start();

    public abstract void showMenu();

    public void showMessage(String message) {
        logger.info(message);
    }
}
