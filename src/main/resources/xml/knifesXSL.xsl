<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    th {
                    background-color: rgb(240, 240, 240);
                    height: 50px;
                    color: black;
                    }
                    td {
                    height: 50px;
                    text-align: center;
                    border-right: 1px solid black;
                    }
                </style>
            </head>
            <body>
                <table style="knifeTable">
                    <tr>
                        <th style="width:200px">Type</th>
                        <th style="width:250px">Handly</th>
                        <th style="width:250px">Origin</th>
                        <th style="width:150px">Length</th>
                        <th style="width:150px">Width</th>
                        <th style="width:250px">Material</th>
                        <th style="width:250px">Handles type</th>
                        <th style="width:250px">Handles subtype</th>
                        <th style="width:150px">Blood flows</th>
                        <th style="width:200px">Value</th>
                    </tr>
                    <xsl:for-each select="knifes/knife">
                        <tr>
                            <td>
                                <xsl:value-of select="type" />
                            </td>
                            <td>
                                <xsl:value-of select="handly" />
                            </td>
                            <td>
                                <xsl:value-of select="origin" />
                            </td>
                            <td>
                                <xsl:value-of select="visual/length" />
                            </td>
                            <td>
                                <xsl:value-of select="visual/width" />
                            </td>
                            <td>
                                <xsl:value-of select="visual/material" />
                            </td>
                            <td>
                                <xsl:value-of select="visual/handles/handlesType" />
                            </td>
                            <td>
                                <xsl:value-of select="visual/handles/handleSubtype" />
                            </td>
                            <td>
                                <xsl:value-of select="visual/bloodFlows" />
                            </td>
                            <td>
                                <xsl:value-of select="value" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>